import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

/* --------------------------------- Fields --------------------------------- */

  title = 'narmetta';

/* ------------------------------- Constructor ------------------------------ */

  constructor(){
    // this language will be used as a fallback when a translation isn't found in the current language
    //translate.setDefaultLang('tr');

    // the lang to use, if the lang isn't available, it will use the current loader to get them
  // translate.use('tr');

  }

/* ---------------------------- Life Cycle Hooks ---------------------------- */

  ngOnInit(): void {

  }
}
