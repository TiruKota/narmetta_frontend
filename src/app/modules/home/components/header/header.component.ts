import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit, HostListener } from '@angular/core';
import { trigger, state, transition, style, animate } from '@angular/animations';
declare var google;
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  animations: [
    trigger('openCloseMenu', [
      state('true', style({ height: '*', opacity: '1' })),
      state('false', style({ height: '0px', opacity: '0'})),
      transition('false <=> true', [animate(500)])
    ])
  ]
})
export class HeaderComponent implements OnInit {

/* --------------------------------- FIelds --------------------------------- */

  isOpen = false;
  megaMenuShow = false;
  opened: boolean;
  isSticky: boolean = false;
  isScrollTop: boolean = false;
  customNavCollapsedHeight = '30px';
  customNavExpandedHeight = '30px';
  languageList = [
    {
      title: 'Telugu',
      abbreviation: 'tr'
    },
  {
    title: 'English',
    abbreviation: 'en'
  }
 ];

  selectedLanguage: any = {
    title: 'Telugu',
    abbreviation: 'tr'
  };
  @HostListener('window:scroll', ['$event'])
  checkScroll() {
    this.isSticky = window.pageYOffset >= 200;
    this.isScrollTop = window.pageYOffset >= 200;
  }

/* ------------------------------- Constructor ------------------------------ */

  constructor(private translate: TranslateService) { }

/* ---------------------------- Life cycle hokks ---------------------------- */

  ngOnInit(): void {
  }


/* ---------------------------- Custom Functions ---------------------------- */

  toggleMenu(): void {
    this.opened = !this.opened;
  }

  menuToggle(): void {
    this.megaMenuShow = !this.megaMenuShow;
  }

  scrollTop(): void {
    (function smoothscroll() {
      var currentScroll =
        document.documentElement.scrollTop || document.body.scrollTop;
      if (currentScroll > 10) {
        window.scrollTo(0, 0);
      }
    })();
  }

  setLanguage(languageObj: any): void {
      this.selectedLanguage = languageObj;
      this.translate.use(this.selectedLanguage.abbreviation);
  }

}
