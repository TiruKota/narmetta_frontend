import { MatTableModule } from '@angular/material/table';
import { TranslateModule } from '@ngx-translate/core';
import { MatMenuModule } from '@angular/material/menu';
import { SharedModule } from './../shared/modules/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SwiperModule } from "ngx-swiper-wrapper";
import {MatSidenavModule} from '@angular/material/sidenav';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { HomeRoutingModule } from './home-routing.module';
import { IndexComponent } from './pages/index/index.component';
import { HomeComponent } from './pages/home/home.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { LightboxModule } from 'ngx-lightbox';
import { AboutVillageComponent } from './pages/about-village/about-village.component';
import { GovernanceComponent } from './pages/governance/governance.component';
import { GovtOfficeComponent } from './pages/govt-office/govt-office.component';
import { EventsComponent } from './pages/events/events.component';
import { PublicPlacesComponent } from './pages/public-places/public-places.component';
@NgModule({
  declarations: [IndexComponent, HomeComponent, HeaderComponent, FooterComponent, AboutVillageComponent, GovernanceComponent, GovtOfficeComponent, EventsComponent, PublicPlacesComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    MatSidenavModule,
    SwiperModule,
    LightboxModule,
    SharedModule,
    PerfectScrollbarModule,
    MatMenuModule,
    MatTableModule,
    TranslateModule
  ]
})
export class HomeModule { }
