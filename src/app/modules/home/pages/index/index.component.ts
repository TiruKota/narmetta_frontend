import { TranslateService } from '@ngx-translate/core';
import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { Lightbox } from 'ngx-lightbox';
import { NgwWowService } from 'ngx-wow';
@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit, AfterViewInit {
  public _albums: Array<any> = [];
  public caption: Array<any> = ['Narmetta Development' , 'Village Infrastructure' , 'Events' , 'Initiatives' ];
  loadAfter = false;
  languageVideo = false;
  @ViewChild("videoTag") videoTagRef: ElementRef;
  public secondSwapper: SwiperConfigInterface = {
    direction: 'horizontal',
    slidesPerView: 2,
    spaceBetween: 0,
    keyboard: true,
    loop: false,
    preloadImages: false,
    // Enable lazy loading
    lazy: false,
    autoplay: false,
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    // breakpoints: {
    //   640: {
    //     slidesPerView: 2,
    //     spaceBetween: 5,
    //   },
    //   768: {
    //     slidesPerView: 4,
    //   },
    //   1200: {
    //     slidesPerView: 5,
    //   },
    // },
    breakpoints: {
      // when window width is >= 240px
      240: {
      slidesPerView: 1,
      spaceBetween: 0
      },
      // when window width is >= 320px
      320: {
      slidesPerView: 2,
      spaceBetween: 0
      },
      // when window width is >= 480px
      480: {
      slidesPerView: 2,
      spaceBetween: 0
      },
      // when window width is >= 640px
      640: {
      slidesPerView: 3,
      spaceBetween: 0
      },
      959:{
        slidesPerView: 4,
        spaceBetween: 0
      },
      1200: {
      slidesPerView: 4,
      spaceBetween: 25
      }
    }
  };
  public govtOffice: SwiperConfigInterface = {
    direction: 'horizontal',
    slidesPerView: 1,
    spaceBetween: 0,
    keyboard: true,
    loop: false,
    preloadImages: false,
    // Enable lazy loading
    lazy: false,
    autoplay: false,
    navigation: {
      nextEl: '.swiper-button-next ',
      prevEl: '.swiper-button-prev',
    },
    // breakpoints: {
    //   640: {
    //     slidesPerView: 2,
    //     spaceBetween: 5,
    //   },
    //   768: {
    //     slidesPerView: 4,
    //   },
    //   1200: {
    //     slidesPerView: 5,
    //   },
    // },
    breakpoints: {
      // when window width is >= 240px
      240: {
      slidesPerView: 1,
      spaceBetween: 0
      },
      // when window width is >= 320px
      320: {
      slidesPerView: 1,
      spaceBetween: 0
      },
      // when window width is >= 480px
      480: {
      slidesPerView: 1,
      spaceBetween: 0
      },
      // when window width is >= 640px
      640: {
      slidesPerView: 3,
      spaceBetween: 0
      },
      959:{
        slidesPerView: 3,
        spaceBetween: 0
      },
      1200: {
      slidesPerView: 3,
      spaceBetween: 25
      }
    }
  };
  public publicWapper: SwiperConfigInterface = {
    direction: 'horizontal',
    slidesPerView: 2,
    spaceBetween: 0,
    keyboard: true,
    loop: false,
    preloadImages: false,
    // Enable lazy loading
    lazy: false,
    autoplay: false,
    // navigation: {
    //   nextEl: '.swiper-button-next',
    //   prevEl: '.swiper-button-prev',
    // },
    // breakpoints: {
    //   640: {
    //     slidesPerView: 2,
    //     spaceBetween: 5,
    //   },
    //   768: {
    //     slidesPerView: 4,
    //   },
    //   1200: {
    //     slidesPerView: 5,
    //   },
    // },
    breakpoints: {
      // when window width is >= 240px
      240: {
      slidesPerView: 1,
      spaceBetween: 0
      },
      // when window width is >= 320px
      320: {
      slidesPerView: 2,
      spaceBetween: 0
      },
      // when window width is >= 480px
      480: {
      slidesPerView: 2,
      spaceBetween: 0
      },
      // when window width is >= 640px
      640: {
      slidesPerView: 3,
      spaceBetween: 0
      },
      959:{
        slidesPerView: 4,
        spaceBetween: 0
      },
      1200: {
      slidesPerView: 4,
      spaceBetween: 25
      }
    }
  };

  public gallerySlider: SwiperConfigInterface = {
    direction: 'horizontal',
    slidesPerView: 2,
    spaceBetween: 0,
    keyboard: true,
    loop: false,
    thumbs: true,
    preloadImages: false,
    // Enable lazy loading
    lazy: false,
    autoplay: false,
    breakpoints: {
      // when window width is >= 240px
      240: {
      slidesPerView: 1,
      spaceBetween: 0
      },
      // when window width is >= 320px
      320: {
      slidesPerView: 1,
      spaceBetween: 0
      },
      // when window width is >= 480px
      480: {
      slidesPerView: 1,
      spaceBetween: 0
      },
      // when window width is >= 640px
      640: {
      slidesPerView: 1,
      spaceBetween: 0
      },
      959:{
        slidesPerView: 1,
        spaceBetween: 0
      },
      1200: {
      slidesPerView: 1,
      spaceBetween: 25
      }
    }
  };
  public galleryThumbs: SwiperConfigInterface = {
    direction: 'horizontal',
    slidesPerView: 5,
    spaceBetween: 15,
    keyboard: true,
    loop: false,
    preloadImages: false,
    // Enable lazy loading
    lazy: false,
    autoplay: false,
    breakpoints: {
      // when window width is >= 240px
      240: {
      slidesPerView: 1,
      spaceBetween: 0
      },
      // when window width is >= 320px
      320: {
      slidesPerView: 2,
      spaceBetween: 0
      },
      // when window width is >= 480px
      480: {
      slidesPerView: 2,
      spaceBetween: 0
      },
      // when window width is >= 640px
      640: {
      slidesPerView: 5,
      spaceBetween: 0
      },
      959:{
        slidesPerView: 5,
        spaceBetween: 0
      },
      1200: {
      slidesPerView: 5,
      spaceBetween: 25
      }
    }
  }

  public touristSlider: SwiperConfigInterface = {
    direction: 'horizontal',
    slidesPerView: 5,
    spaceBetween: 15,
    keyboard: true,
    loop: false,
    preloadImages: false,
    // Enable lazy loading
    lazy: false,
    autoplay: false,
    breakpoints: {
      // when window width is >= 240px
      240: {
      slidesPerView: 1,
      spaceBetween: 0
      },
      // when window width is >= 320px
      320: {
      slidesPerView: 2,
      spaceBetween: 0
      },
      // when window width is >= 480px
      480: {
      slidesPerView: 2,
      spaceBetween: 0
      },
      // when window width is >= 640px
      640: {
      slidesPerView: 4,
      spaceBetween: 0
      },
      959:{
        slidesPerView: 4,
        spaceBetween: 0
      },
      1200: {
      slidesPerView: 4,
      spaceBetween: 25
      }
    }
  }
  public eventsWapper: SwiperConfigInterface = {
    direction: 'horizontal',
    slidesPerView: 1,
    spaceBetween: 0,
    keyboard: true,
    loop: true,
    preloadImages: false,
    // Enable lazy loading
    lazy: false,
    autoplay: true,
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    // breakpoints: {
    //   640: {
    //     slidesPerView: 2,
    //     spaceBetween: 5,
    //   },
    //   768: {
    //     slidesPerView: 4,
    //   },
    //   1200: {
    //     slidesPerView: 5,
    //   },
    // },
    breakpoints: {
      // when window width is >= 240px
      240: {
      slidesPerView: 1,
      spaceBetween: 0
      },
      // when window width is >= 320px
      320: {
      slidesPerView: 1,
      spaceBetween: 0
      },
      // when window width is >= 480px
      480: {
      slidesPerView: 1,
      spaceBetween: 0
      },
      // when window width is >= 640px
      640: {
      slidesPerView: 3,
      spaceBetween: 0
      },
      959:{
        slidesPerView: 2,
        spaceBetween: 0
      },
      1200: {
      slidesPerView: 2,
      spaceBetween: 25
      }
    }
  };


  selectedLanguage = 'tr';

/* ------------------------------- Constructor ------------------------------ */

  constructor(private _lightbox: Lightbox, private wowService: NgwWowService, private translateService: TranslateService) {
    for (let i = 0; i < 4; i++) {
      const src = '../../../../../assets/images/gallery/' + i + '.jpg';
      const caption = this.caption[i];
      const thumb = '../../../../../assets/images/gallery/' + i + '.jpg';
      const album = {
         src: src,
         caption: caption,
         thumb: thumb
      };

      this._albums.push(album);
    }
    this.wowService.init();
    }
      open(index: number): void {
        // open lightbox
        this._lightbox.open(this._albums, index);
      }

      close(): void {
        // close lightbox programmatically
        this._lightbox.close();
      }

/* ---------------------------- Life cycle hooks ---------------------------- */

  ngOnInit(): void {

    this.translateService.onLangChange.subscribe(data => {
      this.selectedLanguage = data.lang;
    });
  }

  ngAfterViewInit(): void {
    this.loadAfter =true;

    setTimeout(()=>{
      this.languageVideo = true;
    },500);
    //  this.videoTagRef.nativeElement.mutated = true;
    //  this.videoTagRef.nativeElement.click();
    //  this.videoTagRef.nativeElement.play();
  }

startVideo(){

}


}
