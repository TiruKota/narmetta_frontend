import { Component, OnInit } from '@angular/core';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
@Component({
  selector: 'app-govt-office',
  templateUrl: './govt-office.component.html',
  styleUrls: ['./govt-office.component.scss']
})
export class GovtOfficeComponent implements OnInit {

  constructor() { }
  public gallerySlider: SwiperConfigInterface = {
    direction: 'horizontal',
    slidesPerView: 2,
    spaceBetween: 0,
    keyboard: true,
    loop: false,
    thumbs: true,
    preloadImages: false,
    // Enable lazy loading
    lazy: false,
    autoplay: false,
    breakpoints: {
      // when window width is >= 240px
      240: {
      slidesPerView: 1,
      spaceBetween: 0
      },
      // when window width is >= 320px
      320: {
      slidesPerView: 1,
      spaceBetween: 0
      },
      // when window width is >= 480px
      480: {
      slidesPerView: 1,
      spaceBetween: 0
      },
      // when window width is >= 640px
      640: {
      slidesPerView: 1,
      spaceBetween: 0
      },
      959:{
        slidesPerView: 1,
        spaceBetween: 0
      },
      1200: {
      slidesPerView: 1,
      spaceBetween: 25
      }
    }
  };
  public galleryThumbs: SwiperConfigInterface = {
    direction: 'horizontal',
    slidesPerView: 5,
    spaceBetween: 15,
    keyboard: true,
    loop: false,
    preloadImages: false,
    // Enable lazy loading
    lazy: false,
    autoplay: false,
    breakpoints: {
      // when window width is >= 240px
      240: {
      slidesPerView: 1,
      spaceBetween: 0
      },
      // when window width is >= 320px
      320: {
      slidesPerView: 2,
      spaceBetween: 0
      },
      // when window width is >= 480px
      480: {
      slidesPerView: 2,
      spaceBetween: 0
      },
      // when window width is >= 640px
      640: {
      slidesPerView: 5,
      spaceBetween: 0
      },
      959:{
        slidesPerView: 5,
        spaceBetween: 0
      },
      1200: {
      slidesPerView: 5,
      spaceBetween: 25
      }
    }
  }
  ngOnInit(): void {
  }

}
