import { PublicPlacesComponent } from './pages/public-places/public-places.component';
import { EventsComponent } from './pages/events/events.component';
import { GovtOfficeComponent } from './pages/govt-office/govt-office.component';
import { GovernanceComponent } from './pages/governance/governance.component';
import { AboutVillageComponent } from './pages/about-village/about-village.component';
import { IndexComponent } from './pages/index/index.component';
import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    pathMatch: 'prefix',
    children: [
      {
        path: '',
        component: IndexComponent,
        pathMatch: 'full'
      },
      {
        path: 'index',
        component: IndexComponent
      },
      {
        path: 'about-village',
        component: AboutVillageComponent
      },
      {
        path: 'governance',
        component: GovernanceComponent
      },
      {
        path: 'govt-office',
        component: GovtOfficeComponent
      },
      {
        path: 'events',
        component: EventsComponent
      },
      {
        path: 'public-places',
        component: PublicPlacesComponent
      }

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
